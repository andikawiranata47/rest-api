package com.example.restapid;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TambahBuku extends AppCompatActivity {
    Button btSimpan;
    TextView tvId;
    TextView tvJudul;
    TextView tvDeskripsi;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tambah_buku);

        btSimpan.findViewById(R.id.btnSimpan);
        tvId.findViewById(R.id.formTambahId);
        tvJudul.findViewById(R.id.formTambahJudul);
        tvDeskripsi.findViewById(R.id.formTambahDeskripsi);

        btSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                PerpustakaanService perpustakaanService = RetrofitClient.getClient().create(PerpustakaanService.class);
                Call<Buku> listRequest = perpustakaanService.setBuku(tvId.getText().toString(),
                        tvJudul.getText().toString(), tvDeskripsi.getText().toString());
                listRequest.enqueue(new Callback<Buku>() {
                    public void onResponse(Call<Buku> call, Response<Buku> response) {
                        if (response.isSuccessful()) {
                            startActivity(i);
                        } else {
                            Log.d("errt", "" + response.errorBody());
                            Toast.makeText(TambahBuku.this, "" + response.errorBody(), Toast.LENGTH_SHORT).show();
                            startActivity(i);
                        }
                    }
                    public void onFailure(Call<Buku> call, Throwable t) {
                        Log.d("DataModel", "" + t.getMessage());
                        Toast.makeText(getApplicationContext(), "Error : " + t.getMessage(), Toast.LENGTH_LONG).show();
                        startActivity(i);
                    }
                });
            }
        });

    }
}
