package com.example.restapid;

import android.provider.ContactsContract;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface PerpustakaanService {
    @GET("buku")
    Call<List<Buku>> listBuku();

    @GET("buku/{id}")
    Call<Buku> getBuku(@Path("id") String id);

    @POST("buku")
    Call<Buku> setBuku(@Field("id") String id,
                       @Field("judul") String judul,
                       @Field("deskripsi") String deskripsi);
}
